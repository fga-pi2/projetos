---
layout: post
title: 2024.1 - ScanPoint
date: 2024-07-12 19:20:00 +0700
description: Modelagem de objetos 3D por infravermelho
img: fotoequipeScan.png # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [ScanPonit, modelagem 3D, digitalização 3D, escaneamento]
comments: false
---

# ScanPoint

## Problema a ser resolvido:

No contexto atual da fabricação digital, a demanda por soluções que simplifiquem e aprimorem o processo de digitalização 3D está em constante crescimento. No âmbito da disciplina de Projeto Integrador 2 da Universidade de Brasília, cujo objetivo é integrar as engenharias eletrônica, energia, aeroespacial, automotiva e de software do campus Gama, surgiu a iniciativa de desenvolver um projeto que viabilize a construção de um scanner. A abordagem escolhida é baseada na utilização de sensores infravermelhos para capturar pontos de distância de objetos físicos, concentrando o controle dos sensores e motores em apenas um Arduino. Posteriormente, os pontos identificados são enviados para um computador, onde são processados para formar uma nuvem de pontos (point cloud), que, por sua vez, é utilizada para gerar um modelo tridimensional.

Este projeto representa uma abordagem que utiliza tecnologias como o Arduino e sensores infravermelhos, tornando o projeto mais acessível e disponível para uma gama mais ampla de usuários, reduzindo as barreiras de entrada para a digitalização 3D. A versatilidade e a capacidade de processamento do Arduino em conjunto com a precisão dos sensores infravermelho são responsáveis pela captura dos pontos de distância do objeto em questão, e, assim, o sistema cria uma representação digital precisa da sua superfície, sem a necessidade de equipamentos complexos ou de alto custo.

Ao adotar uma abordagem que combina acessibilidade, simplicidade e, futuramente, eficiência ao realizar o escaneamento e digitalização 3D, este projeto tem o potencial de democratizar esse processo e torná-lo utilizável a uma ampla gama de usuários, desde entusiastas e estudantes até profissionais e empresas. Além disso, ao utilizar tecnologias de código aberto, incentiva-se a colaboração e o desenvolvimento contínuo, criando um ecossistema de inovação compartilhado. Este documento tem como objetivo apresentar o ScanPoint em detalhes, destacando seus recursos, benefícios e aplicações.

## Visão geral da solução

Este projeto dedica-se à construção de um sistema de digitalização 3D denominado ScanPoint, que simplifica e aprimora o processo de captura e reprodução tridimensional de objetos físicos. O sistema é composto por uma mesa scanner equipada com um Arduino e sensores infravermelho, e um aplicativo desktop responsável pelo processamento dos dados capturados e geração de arquivos STL para impressão 3D.

O ScanPoint foi concebido para atender à crescente demanda por soluções acessíveis e eficientes na digitalização 3D, que facilitem a produção de modelos tridimensionais de alta precisão. A atual complexidade e custo elevado dos equipamentos de digitalização 3D limitam seu uso a indústrias específicas, enquanto o ScanPoint visa democratizar essa tecnologia, tornando-a acessível a um público mais amplo, incluindo pequenos fabricantes, educadores e entusiastas.

O objetivo principal do projeto é desenvolver um sistema que permita a captura eficiente de dados de objetos físicos e a geração de modelos 3D prontos para impressão. Isso inclui:

- Proporcionar uma interface intuitiva para os usuários.
- Garantir a precisão e a qualidade dos modelos 3D gerados.
- Tornar o processo de digitalização 3D mais acessível e rápido.
- Oferecer uma solução robusta e confiável para diversas aplicações.


## Banner:
Acesse o banner do projeto: [ScanPoint](https://www.canva.com/design/DAGJR7PKIO4/qaewzKEAcOWhN0B7CRS70w/view?utm_content=DAGJR7PKIO4&utm_campaign=designshare&utm_medium=link&utm_source=editor)

## Time

| Área         | Cargo                               | Nome                                                                 | ID         |
|--------------|-------------------------------------|----------------------------------------------------------------------|------------|
| Software     | **Coordenadora geral**              | [Carla Rocha](https://gitlab.com/Carlacangussu)                      | 170085023  |
| Software     | **Diretora de qualidade**           | [Brenda Santos](https://gitlab.com/brendavsantos)                    | 180041444  |
| Software     | **Diretor técnico de Software**     | [Denniel William](https://gitlab.com/dennielwilliam)                 | 170161871  |
| Aeroespacial | **Diretora técnica de Estrutura**   | [Maria Claudia](https://gitlab.com/mariaclaudialgaspar)              | 200041193  |
| Energia      | **Diretora técnica de Eletrônica/Energia** | [Carolina Oliveira](https://gitlab.com/carolinaroliveira02) | 190011483  |
| Aeroespacial | Desenvolvedor                       | [Cássio Filho](https://gitlab.com/Calcioft)                          | 190011521  |
| Automotiva   | Desenvolvedor                       | [Diogo Soares](https://gitlab.com/soaressc321)                       | 190012188  |
| Eletrônica   | Desenvolvedor                       | [Miguel Munoz](https://gitlab.com/migueleparra)                      | 221008795  |
| Energia      | Desenvolvedor                       | [Lucas Pantoja](https://gitlab.com/lcs.pantoja.silva)                | 190060352  |
| Software     | Desenvolvedora                      | [Ana Carolina](https://gitlab.com/AnaCarolinaRodriguesLeite)         | 190101792  |
| Software     | Desenvolvedor                       | [Guilherme Basilio](https://gitlab.com/GuilhermeBES)                 | 160007615  |
| Software     | Desenvolvedor                       | [Artur de Souza](https://gitlab.com/art_42)                          | 190010606  |
| Software     | Desenvolvedor                       | [Ciro Costa](https://gitlab.com/ciro-c)                              | 190011611  |
| Software     | Desenvolvedor                       | [Vinicius Vieira](https://gitlab.com/viniciusvieira00)               | 190118059  |
| Software     | Desenvolvedor                       | [Pedro Menezes](https://gitlab.com/pemiinem)                         | 190139323  |


## Fotos 
<img src="../assets/img/foto1.jpeg" alt="imagem1" width="350" height="350">
<img src="../assets/img/foto2.jpeg" alt="imagem2" width="350" height="350">
<img src="../assets/img/foto3.jpeg" alt="imagem3" width="350" height="350">


## Vídeos de funcionamento:
[![](http://img.youtube.com/vi/v-eVb3_VNGw/0.jpg)](https://www.youtube.com/watch?v=v-eVb3_VNGw "Video comercial scanpoint")

## Repositório: 
[Link para a solução](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-08/scanpoint/)