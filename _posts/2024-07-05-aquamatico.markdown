---
layout: post
title: 2024.1 - Aquamático
date: 2024-07-12 21:10:17 +0700
description: Automação de um aquário
tags: [Aquamatico, monitoramento, pH, Alimentação, AUtomação]
comments: false
---

# Aquamático

## Problema a ser resolvido:

O projeto Aquamático surge da necessidade de automatizar a manutenção e alimentação de animais aquáticos, visando garantir precisão e cuidado contínuo com esses seres vivos. Com a crescente demanda por soluções que facilitem a rotina dos cuidadores, especialmente em um contexto em que a sociedade busca maneiras de otimizar o tempo e garantir o bem-estar dos animais, o Aquamático se apresenta como uma resposta eficaz a esses desafios.

Dada a possibilidade dos tutores desses animais serem acometidos por esquecimento de alguns cuidados, diante da suas demandas, e falhas na quantidade de comida a se dada e limpeza a serem realizadas, o Aquamático é uma solução que pode apoiar nisso através das suas automações, a exemplo de alimentação automática e monitoramento geral das condições do ambiente.

Nesse sentido, essa solução foi concebida para oferecer uma maneira eficiente e precisa de cuidar dos animais aquáticos, garantindo uma vida saudável e segura para eles. Ao automatizar tarefas como trocas parciais de água, medição de parâmetros vitais e dispersão de alimentos, o Aquamático visa proporcionar aos cuidadores a tranquilidade de saber que seus animais estão sendo cuidados de forma adequada, mesmo em sua ausência.

Assim, o Aquamático destaca-se como uma solução inovadora que agrega valor à sociedade, promovendo o bem-estar dos animais aquáticos e facilitando a rotina dos cuidadores. Ao oferecer uma abordagem automatizada e precisa para a manutenção e alimentação de animais aquáticos, o projeto visa atender às necessidades crescentes desse mercado, contribuindo para uma convivência harmoniosa entre os tutores e animais aquáticos.


## Visão geral da solução

Manter um aquário é uma tarefa desafiadora e ao mesmo tempo recompensadora, simular um ambiente aquático de forma natural em um cômodo é algo que requer bastante controle. Apesar de todos os desafios, a tarefa de cuidar de um aquário é simples porém trabalhosa.

No aquário existem diversas variáveis que influenciam no desenvolvimento e equilíbrio de um sistema. Por exemplo: pH, temperatura, amônia, nitrito e nível da água. A influência do pH em um aquário irá definir quais espécies de peixes, invertebrados e plantas o aquário poderá ter.

Quando realizamos testes desses parâmetros citados acima, dependendo dos valores obtidos e esperados devem ser tomadas certas providências. Por exemplo se a temperatura medida for acima do esperado, deve realizar a resfriamento do aquário. Se caso o pH esperado for acima de 7 e quando medido for próximo de 6 então deve ser adicionado um químico que aumente a reserva alcalina.

Diante disso, o nosso projeto irá abordar as seguintes questões:

- Monitorar parâmetros da água: Os parâmetros que serão medidos serão pH, temperatura e nível de água. Com essas informações será possível saber qual ação deverá ser tomada para retornar para as condições esperadas.

- Gerenciar alimentação: Será possível marcar de quantas em quantas horas deverá ser liberado a alimentação para os habitantes do aquário. Dessa forma, será possível evitar sobras de alimentos que acabam influenciando na qualidade da água.

- Gerenciar intensidade de luz: A intensidade de luz e o tempo que ela está ligada em um aquário deve ser controlado para não causar estresse na fauna encontrada no aquário e precaver o surgimento de algas indesejadas.

- Gerenciar nível de água: A água presente em um aquário evapora com o dercorrer do tempo. Além disso semanalmente é necessário a realização de trocas parciais de água a fim de manter a qualidade da água.

## Banner

Acesse o banner do projeto [clicando aqui.](https://www.canva.com/design/DAGJt5jETfw/HzQUzcM4NBsJmq7Jc_I6JQ/edit?utm_content=DAGJt5jETfw&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)

## Time 

| Nome                                      | Matrícula | Curso                   |
| ----------------------------------------- | --------- | ----------------------- |
| Amanda Cristina Teixeira Pinto            | 190041935 | Engenharia Eletrônica   |
| Antônio Ferreira de Castro Neto           | 190044799 | Engenharia de Software  |
| Arthur Talles de Sousa Cunha              | 190054832 | Engenharia de Software  |
| Eduardo Gurgel Pereira de Carvalho        | 190045485 | Engenharia de Software  |
| Gabriel Tablas Miranda                    | 190028173 | Engenharia Aeroespacial |
| Henrique Sandoval Camargo Hida            | 180113569 | Engenharia de Software  |
| Isadora da Cruz Galvão dos Santos Soares  | 180122606 | Engenharia de Software  |
| Kesia Ramos da Silva                      | 180076523 | Engenharia Eletrônica   |
| Luana Bruna Lopes Ribeiro                 | 200022661 | Engenharia Aeroespacial |
| Luiz Henrique Fernandes Zamprogno         | 190033681 | Engenharia de Software  |
| Nathaniel Luis Pedroso Grimm              | 170126404 | Engenharia Automotiva   |
| Paulo Geraldo Souza Neto                  | 190018658 | Engenharia de Energia   |
| Pedro Victor Lima Torreão                 | 190036761 | Engenharia de Software  |
| Serena Giovana Nóbrega Vieira             | 180114611 | Engenharia de Energia   |
| Yan Andrade de Sena                       | 180145363 | Engenharia de Software  |


## Fotos

<img src="../assets/img/aquario.jpeg" alt="imagem2" width="350" height="350">
<img src="../assets/img/aquario-aberto.jpg" alt="imagem2" width="350" height="350">

## Vídeo de Funcionamento

Acesse o vídeo de funcionamento do projeto [clicando aqui.](https://youtu.be/Qh5x09jjdws?si=nOgnYfYzIe9ngit8)

## Repositórios

- [Link para o relatório](https://relatorio-fga-pi2-semestre-2024-1-grupo05-4beaf972d28f4e6162d41.gitlab.io/)
- [Link para a solução de software e sistema embarcado](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software)

