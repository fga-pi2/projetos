---
layout: post
title: 2024.1 - Helios CW1
date: 2024-07-07 12:00:00 +0700
description: Helios CW1 - Limpador de Painéis Solares
img: helios.jpg # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [Painél solar, Limpador de placa solar, energia limpa, energia solar]
comments: false
---

# Helios CW1 - Limpador de Painéis Solares

## Problema a ser resolvido

Desde a segunda metade da década passada, a utilização de painéis solares para geração de energia elétrica em instalações residenciais, industriais e comerciais de pequeno, médio e grande porte vem crescendo de forma significativa. Essa geração já representa, segundo a Empresa de Pesquisa Energética (EPE), 4,4% da geração elétrica do país, e 3,6% da geração elétrica global.

Esta participação crescente da geração com placas solares gera a necessidade de entendimento dos fatores que mais afetam a produtividade de instalações, visando o aumento da produção, e assim o rendimento da instalação. Identifica-se que "a sujeira nos sistemas de placas solares devido à poeira e à neve", e a subsequente diminuição na produção de energia, é o fator de maior impacto sobre a produção do sistema depois da irradiação solar Estas perdas devido à sujeira em painéis solares representaram em 2018 uma diminuição de 4% da produção anual, correspondendo a uma perda de 3 a 5 bilhões de euros, estimando que este valor poderia chegar a 7 bilhões de euros em 2023.

Então, o projeto busca desenvolver uma solução automática, e que requeira o mínimo de intervenção do operador durante o seu funcionamento, para limpeza de painéis solares, visando melhorar a produção enérgica, reduzindo a sujeira nos painéis.

## Visão geral da solução

Tem-se neste projeto o objetivo de conceber e construir um protótipo do limpador de placas solares que se movimente sobre a placa para limpá-la de forma completa, bem como elaborar o seu sistema de controle de forma que um operador possa operar o limpador de forma simples e ágil, por meio de um aplicativo remoto.

Assim, a solução tem 3 sistemas:

- **Software**: responsável pela criação do aplicativo para controle e monitoramento dos limpadores. Além disso, a programação para funcionamento da parte de eletrônica (ESP32 e atuadores/transdutores).
- **Elétrico**: responsável pelo funcionamento da bateria e da construção eletrônica da solução.
- **Estrutural**: responsável pela construção estrutural e mecânica do limpador, fazendo todos os outros sistemas serem comportados no produto e que sua movimentação seja possível.

## Poster

Acesse o poster do projeto [aqui](https://www.canva.com/design/DAGJJieijJY/inxLkn6dK8z8yVRd9UxGZA/edit?utm_content=DAGJJieijJY&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton).

## Time

| Nome                                 | Matrícula | Curso |
|--------------------------------------|-----------|-------|
| Bernardo Chaves Pissutti             | 190103302 |Engenharia de Software|
| Christian Fleury Alencar Siqueira    | 190011602 |Engenharia de Software|
| Cícero Barrozo Fernandes Filho       | 190085819 |Engenharia de Software|
| Daniel Alberto dos Santos Filho      | 180030990 |Engenharia Aeroespacial|
| Gabriel Barbosa Pfeislticker de Knegt| 180101056 |Engenharia Aeroespacial|
| Gabriel Roger Amorim da Cruz         | 200018248 |Engenharia de Software|
| Guilherme Sanchez Dutra              | 180113500 |Engenharia de Energia|
| João Pedro de Camargo Vaz            | 200020650 |Engenharia de Software|
| Lucas Pinheiro de Souza              | 160156866 |Engenharia Eletrônica|
| Matheus Matos Fernandes              | 170111156 |Engenharia de Energia|
| Matheus Soares Arruda                | 190093480 |Engenharia de Software|
| Patrick Christian de Melo            | 180036432 |Engenharia Aeroespacial|
| Thiago Siqueira Gomes                | 190055294 |Engenharia de Software|
| Victor Hugo Oliveira Leão            | 200028367 |Engenharia de Software|
| Vinicius Assumpcao de Araújo         | 200028472 |Engenharia de Software|

## ⁠Fotos

<img src="../assets/img/helios.jpg" alt="imagem1" width="350" height="350">
<img src="../assets/img/render1_pc2.jpg" alt="imagem2" width="350" height="350">
<img src="../assets/img/render2_pc2.jpg" alt="imagem3" width="350" height="350">

## Vídeo de funcionamento

Veja o vídeo de funcionamento do projeto [aqui](https://drive.google.com/file/d/1LnbkogH1p_DUBruOQb6LEUHiM07NkcFI/view?usp=sharing).

## Repositórios

Todos os repositórios do projeto podem ser encontrados [aqui](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-05).

O relatório está presente [aqui](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-05/relatorio/).
