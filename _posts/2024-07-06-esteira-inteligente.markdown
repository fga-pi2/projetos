---
layout: post
title: 2024.1 - Esteira Inteligente
date: 2024-07-12 22:32:20 +0700
description: Esteira Inteligente com reconhecimento de componentes por IA
img: esteira-final2.jpg # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [Esteira Inteligente, Inteligência Artificial, Monitoramento, Analise de imagem]
comments: false
---

# Esteira Inteligente

## Problema a ser resolvido:

Ao longo da linha de produção, é comum ocorrerem problemas de montagem. Um dos desafios enfrentados por indústrias com uma vasta gama de produtos são as pequenas alterações nos projetos, que para as equipes de Qualidade e Engenharia são extremamente relevantes, podendo afetar a experiência do usuário ou modificar integralmente as estruturas físicas dos produtos.

Entretanto, essas modificações podem passar despercebidas por operadores e gestores de produção, em virtude do grande volume de variedades. Nesse contexto, a Esteira Inteligente surge como uma solução eficaz, agilizando a alteração ou criação de novos produtos, prevenindo lacunas no estoque e aumentando o valor rotativo do produto, especialmente quando uma simples modificação de um componente pode resulta em uma alteração substancial do produto.

## Visão geral da solução

Através de um sistema de esteiras, a Esteira Inteligente transporta peças previamente determinadas pela equipe de Engenharia e Qualidade, utilizando um sistema de seleção integrado em rede local. Durante o funcionamento, a máquina verifica as peças dos conjuntos por meio de um sistema de câmeras que utilizando de inteligência artificial para validação das peças, averiguação de massa das peças através de uma balança.

É importante destacar que a Esteira Inteligente não se destina ao uso doméstico. Trata-se de um sistema de separação de peças, embora contenha divisórias, sendo necessário o fornecimento de alimentação de forma contínua para o seu funcionamento.


## Poster


Acesse o poster do projeto [aqui.](https://www.canva.com/design/DAGJ7tDG5o0/KHPG3BLVqULU6Xz5hLCyMg/edit?utm_content=DAGJ7tDG5o0&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)

## Time 

| Nome                                      | Matrícula | Curso                   |
| ----------------------------------------- | --------- | ----------------------- |
| Ana Paula de Souza de Araujo              | 190024577 | Engenharia Aeroespacial  |
| Caio Sampaio de Carvalho e Silva          | 190085321 | Engenharia de Energia |
| Danilo Domingo Vitoriano Silva            | 180015311 | Engenharia de Software  |
| Fernanda Noronha Coelho dos Santos Marques| 190027762 | Engenharia Aeroespacial  |
| Heitor Marques Simões Barbosa             | 202016462 | Engenharia de Software  |
| Jefferson França Santos                   | 180102761 | Engenharia de Software  |
| Joel Jefferson Fernandes Moreira          | 190043083 | Engenharia Eletrônica   |
| Lucas Alexandre Gama Correia              | 180149741 | Engenharia Aeroespacial |
| Matheus Henrick Dutra dos Santos          | 190018101 | Engenharia de Software   |
| Matheus Phillipo Silverio Silva           | 150154348 | Engenharia de Software   |
| Mauricio Maiyson Hoffman de Melo          | 160138574 | Engenharia Eletrônica   |
| Michael Emanuel Silva Costa               | 190035528 | Engenharia de Energia |


## Fotos



<img src="https://esteira-inteligente-docs-fga-pi2-semestre-2024-1-4f19bee6666f37.gitlab.io/assets/images/estruturas/esteira-final-quadrado.jpg" alt="imagem1" width="350" height="350">
<img src="https://esteira-inteligente-docs-fga-pi2-semestre-2024-1-4f19bee6666f37.gitlab.io/assets/images/estruturas/canto-final.jpg" alt="imagem2" width="350" >
<img src="https://esteira-inteligente-docs-fga-pi2-semestre-2024-1-4f19bee6666f37.gitlab.io/assets/images/estruturas/balanca-quadrada.jpg" alt="imagem3" width="350" height="350">
<img src="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-frontend/-/raw/main/public/kit1.jpg?ref_type=heads" alt="imagem4" width="350" height="250">


## Vídeos de funcionamento

[Aqui](https://www.youtube.com/watch?v=_agGI31cu70)

## Repositório

 [Link para a solução](https://esteira-inteligente-docs-fga-pi2-semestre-2024-1-4f19bee6666f37.gitlab.io)