# Projetos de PI2

O link para o gitpage pode ser acessado [aqui](https://lappis-unb.gitlab.io/fga-pi2/projetos/)
### Passos para Contribuir com seu Relatório

1. **Fork o Repositório**
   - Acesse o repositório desejado no GitHub.
   - Clique no botão "Fork" no canto superior direito da página.

2. **Clone o Repositório na Sua Máquina Local**
   - Abra seu terminal ou prompt de comando.
   - Digite o seguinte comando para clonar o repositório forkado:
     ```sh
     git clone https://github.com/seu-usuario/nome-do-repositorio.git
     ```
   - Substitua `seu-usuario` pelo seu nome de usuário do GitHub e `nome-do-repositorio` pelo nome do repositório.

3. **Adicione seu Relatório na Pasta `_posts`**
   - Navegue até a pasta do repositório clonado:
     ```sh
     cd nome-do-repositorio
     ```
   - Adicione seu relatório na pasta `_posts`. Use o template fornecido na pasta e não se esqueça de adicionar a data no nome do arquivo. O nome do arquivo deve ter o formato `YYYY-MM-DD-titulo-do-relatorio.markdown`.

4. **Construa e Pré-Visualize seu Post**
   - Após adicionar seu relatório, você pode construir o site e pré-visualizar seu post:
     ```sh
     bundle exec jekyll build
     ```
     ou
     ```sh
     jekyll build
     ```
   - O HTML gerado será salvo na pasta `_site`.

   - Para pré-visualizar o post no seu navegador, execute:
     ```sh
     bundle exec jekyll serve
     ```
     ou
     ```sh
     jekyll serve
     ```
   - Abra seu navegador e acesse `http://localhost:4000/reports/` para ver o post.

5. **Faça um Pull Request**
   - Após estar satisfeito com seu post, adicione as alterações ao Git, faça commit e envie para o seu repositório forkado:
     ```sh
     git add .
     git commit -m "Adiciona relatório de [data]"
     git push origin main
     ```
   - Acesse o repositório original no GitHub.
   - Clique no botão "Compare & pull request" para criar um pull request com suas alterações.

### Instalando Jekyll
Se você ainda não tem o Jekyll instalado, siga as instruções de instalação disponíveis na [documentação oficial do Jekyll](https://docs.github.com/en/github/working-with-github-pages/testing-your-github-pages-site-locally-with-jekyll).

---

Se precisar de mais ajuda, estou à disposição!